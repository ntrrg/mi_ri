#!/bin/sh

docker-entrypoint.sh --default-authentication-plugin=mysql_native_password &

DATABASE="$MYSQL_USER:$MYSQL_PASSWORD@/$MYSQL_DATABASE"
mi_ri-backend \
  --url "$BASEURL_BACKEND" --db "$DATABASE" --gcid "$GCID" --gcs "$GCS" \
  --grurl "$GRURL" --smtphost "$SMTP_HOST" --smtpport "$SMTP_PORT" \
  --smtpemail "$SMTP_EMAIL" --smtppasswd "$SMTP_PASSWORD" --debug &

exec dist/mi_ri-frontend \
  --addr ":$PORT" --url "$BASEURL_FRONTEND" --api "$BASEURL_BACKEND" $@


FROM golang:1.11.2-alpine3.8 AS build-backend
RUN apk update && apk add git make
WORKDIR /src
COPY backend .
RUN make

FROM golang:1.11.2-alpine3.8 AS build-frontend
RUN apk update && apk add git make
WORKDIR /src
COPY frontend .
RUN make

FROM mysql:8.0.13
COPY --from=build-backend /src/dist /bin
COPY --from=build-frontend /src /miri
WORKDIR /miri
COPY miri-entrypoint.sh .
ENTRYPOINT ["./miri-entrypoint.sh"]

